# How to debloat a Xiaomi phone (MIUI 12 and lower)

## Prerequisites

### ***Windows***

+ **Android USB drivers:**  
    1. [Download](https://developer.android.com/studio/run/win-usb)
    2. Extract the archive
    3. Connect your phone to your PC
    4. Open Device Manager by right clicking the Windows icon on the taskbar
    5. Expand the "Portable Devices" section (if your phone is not there, just check all the other sections
  and also check the connectivity between your phone and PC and then try again)
    6. Right-click your phone
    7. Click "Update driver"
    8. Click "Browse for drivers in my computer"
    9. Now you should see a "Browse..." button, click it and navigate to your extracted folder with the USB drivers
    10. Click "OK"
    11. Click "Next"
    12. Now your USB drivers should be installed
+ **ADB tools:** 
    1. [Download](https://developer.android.com/studio/releases/platform-tools)
    2. Download the version for Windows and extract the archive.

### ***Linux***

+ *Note that you don't need USB drivers on Linux or macOS.*  
+ **ADB tools:** 
    1. [Download](https://developer.android.com/studio/releases/platform-tools)
    2. Download the version for Linux and extract the archive.

### ***macOS***

+ *Note that you don't need USB drivers on Linux or macOS.*  
+ **ADB tools:** 
    1. [Download](https://developer.android.com/studio/releases/platform-tools) 
    2. Download the version for macOS and extract the archive.

### ***Phone***

+ **F-Droid:** [Download](https://f-droid.org)  
    + *Note that if you cannot find the app you are looking for on F-Droid, you can install [Aurora Store](https://f-droid.org/en/packages/com.aurora.store), which is a frontend client for the Google Play store with the option to download apps without having a Google account. Refer to its documentation for help.* 
+ **A web browser:** I recommend [Bromite](https://bromite.org), [Brave](https://brave.com) or [Mull](https://f-droid.org/en/packages/us.spotco.fennec_dos)  
+ **Simple keyboard:** [F-Droid Download](https://f-droid.org/en/packages/rkr.simplekeyboard.inputmethod)  
+ **Enable USB debugging:**  
    1. Go to settings, about phone
    2. Start clicking on the "MIUI Version" button until it shows a message that says "Voilà, you are now a developer!"
    3. Go back to settings, click on "Additional settings", "Developer options" and scroll down until you can see a toggle labeled "USB debugging" and toggle it on.
+ *Note that the code below is going to remove some apps you might not want to be removed, like Android Calendar, but don't worry, since you can install anything later on from F-Droid.*

## Debloating from a Windows PC:

1. Open the folder containing the ADB tools and left click in the address bar with the path to the folder on the top of the window.
2. Type `cmd.exe` (*this will open command prompt inside that folder*).
3. Into the command prompt type `adb devices`.

   *You should see something like:*

   `List of devices attached`

   `b71ab4796d23    unauthorized`
4. Move over to your phone, and authorize the PC to make changes to the phone (the phone should prompt you).
5. Again type `adb devices`.

   *You should see something like:*

   `List of devices attached`

   `b71ab4796d23    device`
6. Download the [debloat.bat](https://gitlab.com/bePrivate/xiaomiDebloat/blob/main/debloat.bat) file, move it into the folder containing the ADB tools and execute it as administrator.
7. Once it shows `DONE` in the command prompt, you can close it and disconnect your phone from your PC.
8. You should now have a debloated Xiaomi phone.

## Debloating from a Linux PC:

### GUI Method

1. Open the folder containing the ADB tools and right click in the File Manager window and then click "Open in Terminal". 
2. Into the Terminal type `./adb devices`.

   *You should see something like:*

   `List of devices attached`

   `b71ab4796d23    unauthorized`
3. Move over to your phone, and authorize the PC to make changes to the phone (the phone should prompt you).
4. Again type `./adb devices`.

   *You should see something like:*

   `List of devices attached`

   `b71ab4796d23    device`
5. Download the [debloat.sh](https://github.com/mnjx/xiaomiDebloat/blob/main/debloat.sh) file, move it into the folder containing the ADB tools and execute it with sudo privileges.
6. Once it shows `DONE` in the Terminal, you can close it and disconnect your phone from your PC.
7. You should now have a debloated Xiaomi phone.

### CLI Method

1. Navigate into the folder containing the ADB tools.  
2. Type `./adb devices`.                                                                                           *You should see something like:*                                                                                `List of devices attached`                                                                                      `b71ab4796d23    unauthorized`   
3. Move over to your phone, and authorize the PC to make changes to the phone (the phone should prompt you).  
4. Again type `./adb devices`.                                                                                     *You should see something like:*                                                                                `List of devices attached`                                                                                      `b71ab4796d23    device`   
5. Download the [debloat.sh](https://gitlab.com/bePrivate/xiaomiDebloat/blob/main/debloat.sh) file, move it into the folder containing the ADB tools and execute it with sudo privileges.  
6. Once it shows `DONE`, you can close it and disconnect your phone from your PC.  
7. You should now have a debloated Xiaomi phone.  

## Debloating from a macOS PC:

1. Open the folder containing the ADB tools and right click in the File Manager window and then click "Open in Terminal". 
2. Into the Terminal type `./adb devices`.

   *You should see something like:*

   `List of devices attached`

   `b71ab4796d23    unauthorized`
3. Move over to your phone, and authorize the PC to make changes to the phone (the phone should prompt you).
4. Again type `./adb devices`.

   *You should see something like:*

   `List of devices attached`

   `b71ab4796d23    device`
5. Download the [debloat.sh](https://gitlab.com/bePrivate/xiaomiDebloat/blob/main/debloat.sh) file, move it into the folder containing the ADB tools and execute it with sudo privileges.
6. Once it shows `DONE` in the Terminal, you can close it and disconnect your phone from your PC.
7. You should now have a debloated Xiaomi phone.

