# Debloat a Xiaomi phone running MIUI 12 or lower.  

## The complete tutorial is in the [xiaomiDebloat.md](https://gitlab.com/bePrivate/xiaomiDebloat/blob/main/xiaomiDebloat.md) file.

## *Note that this tutorial may also work for phones with MIUI version higher than 12. Not all bloatware may be removed.*
